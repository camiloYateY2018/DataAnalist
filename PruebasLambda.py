


class Empleado:
    def __init__(self, nombre, cargo,salario):
        self.nombre = nombre
        self.cargo = cargo
        self.salario = salario

    def __str__(self):
        return "{} que trabaja como {} tiene salario de $ {} ".format(self.nombre, self.cargo, self.salario)

listaEmpleados = [

    Empleado("Juan", "Director", 750000),
    Empleado("Morales", "Arquitecto", 800000),
    Empleado("Antonio", "UX-UI", 1500000),
    Empleado("Mariela", "DBA", 1800000),
    Empleado("Gonzales", "Frontend", 2000000)
]


def funcionLambda():

    # funciones lambda

    salarios_altos = filter(lambda empleado:empleado.salario> 1000000, listaEmpleados)

    # iteramos sobre los resultados

    for salario in salarios_altos:
        print(salario)

# *************************** Funciones Lambda
# forma clasica
def area_trinagulo(base, altura):
    return (base*altura)/2

triangulo1 = area_trinagulo(5,7)

# forma lambda

# *****nombre del dato*** =  **funcion(paramtros)(retorno)
area_trinagulo_lambda     =   lambda base, altura:(base*altura/2)

if __name__ == "__main__":
    funcionLambda()